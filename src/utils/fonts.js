// all fonts used
export default {
  fontFamily_Android: 'IRANSans',
  fontFamilyBold_Android: 'IRANSansBold',
  fontFamilyMedium_Android: 'IRANSansMedium',
  fontFamilyLight_Android: 'IRANSansLight',
  fontFamily_Ios: 'IRANSansMobileFaNum',
};
