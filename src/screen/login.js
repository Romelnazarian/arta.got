import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import Idcard from 'react-native-vector-icons/AntDesign';
import Mobile from 'react-native-vector-icons/FontAwesome';
import allStyle from '../utils/allStyle';
import InputComponent from '../component/inputComponent';
import {validate} from '../utils/validation';
import Icon from 'react-native-vector-icons/FontAwesome';
import NetInfo from '@react-native-community/netinfo';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const Login = (props) => {
  // props.setloading(false);
  const [name, setname] = useState();
  const [nameMessage, setnameMessage] = useState();

  const [phone, setphone] = useState();
  const [PhoneMessage, setPhoneMessage] = useState();

  const [net, setNet] = useState();
  const [error, setError] = useState();


  const checkCode = () => {
    NetInfo.addEventListener((state) => {
      if (state.isConnected) {
        // setTimeout(() => {
        //     props.navigation.navigate('Index')
        // },4000);
        setNet(true);
        // props.navigation.navigate('Index')

        fetch('http://37.152.181.14:300/api/otp/start', {
          method: 'POST',
          // headers: {
          //   Accept: 'application/json',
          //   'Content-Type': 'application/json',
          // },
          body: JSON.stringify({mobileNo: phone}),
        })
          .then((response) => response.json())
          // if (res.status === 'success') {
          //     console.warn(res)
          //   return  props.navigation.navigate('Index')
          .then((responseJson) => {
            if (responseJson.status == 'success') {
              // console.warn(responseJson);
              // AsyncStorage.setItem('login', JSON.stringify(1));
              props.navigation.navigate('OtpLogin');
            } else {
              setError(responseJson.message[0]);
            }
          });
      } else {
        setNet(false);
        setError('لطفا ابتدا اینترنت خود را روشن کنید سپس مجددا تلاش کنید');
      }
    });
  };



  const arrayValue = [
    {
      id: 1,
      nameField: ['phone'],
      value: phone == undefined ? '' : phone,
    },
  ];

  const checkValue = () => {
    let all = true;
    arrayValue.map((item) => {
      let id = item.id,
        value = item.value,
        nameField = item.nameField;
      let resp = [null, null];
      nameField.map((item) => {
        let v = validate(item, value, 'en');
        let checkValidation = v[0];
        let checkValidateMessage = v[1];
        resp[0] = resp[0] || checkValidation ? true : false; // validate is true or false
        resp[1] = checkValidateMessage; // message error
      });
      if (resp[0]) {
        if (id === 1) {
          // name
          //   props.userAction('Code', code);
        } else {
          // phone
          //   props.userAction('phone', phone);
        }
      } else {
        all = false;
        if (id === 1) {
          // name
          setPhoneMessage(resp[1]);
        }
      }
    });

    return all;
  };

  const btn = () => {
    if (checkValue()) {
      checkCode()
      
    }
  };
  return (
    <View style={[styles.container]}>
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={styles.box}>
          <Image
            source={require('../images/imageicon.png')}
            style={{
              width: 150,
              height: 150,
              alignSelf: 'center',
            }}
          />
          <Text
            style={[
              styles.text,
              {textAlign: 'center', fontSize: 22, color: colors.blue},
            ]}>
            نرم افزار ورود
          </Text>

          <InputComponent
            placeholder={'کد ورود به نرم افزار'}
            keyboardType={'numeric'}
            maxLength={11}
            onChangeInput={(value) => setphone(value)}
            style={{
              alignSelf: 'center',
              borderWidth: 2,
              borderRadius: 8,
              height: 55,
              borderColor: colors.blue,
              width: '90%',
              backgroundColor: colors.white,
            }}
            stylelable={{color: colors.gray, textAlign: 'right'}}
            errorMessage={PhoneMessage}
            input={{textAlign: 'center', fontSize: 18}}
            eror={{paddingRight: '5%'}}
          />

            <ButtonComponent
              buttonStyle={{
                borderRadius: 10,
                backgroundColor: colors.blue,
                borderColor: colors.blue,
                alignSelf:'center'
              }}
              onClick={() => btn()}
              titleStyle={{fontSize: 16}}
              titleStyle={{color: colors.white}}
              title={'ورود'}
            />
        </View>
        <View
          style={{
            alignSelf: 'center',
            width: '90%',
            height: 50,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '40%',
              height: 2,
              backgroundColor: colors.lightblue,
            }}></View>
          <Text style={[styles.text, {colors: colors.lightblue}]}>QRCODE</Text>
          <View
            style={{
              width: '40%',
              height: 2,
              backgroundColor: colors.lightblue,
            }}></View>
        </View>
        <TouchableOpacity>
          <Image
            source={require('../images/qrcode.png')}
            style={{
              width: 100,
              height: 100,
              alignSelf: 'center',
            }}
          />
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
  },

  title: {
    ...allStyle.textBold,
    color: colors.gray,
    fontSize: 16,
  },
  text: {
    ...allStyle.text,
    color: colors.navyblue,
    fontSize: 16,
  },
  box: {
    marginTop: '8%',
    width: '95%',
    alignSelf: 'center',
  },
  register: {
    // backgroundColor:'red',

    marginTop: '5%',
    alignSelf: 'center',
    width: '65%',
  },

  box1: {
    padding: 20,
    backgroundColor: colors.green,
    shadowColor: '#c5eff4',
    shadowOpacity: 0.8,
    shadowRadius: 12,
    shadowOffset: {
      height: 1,
      width: 1,
    },

    width: 130,
    height: 130,
    elevation: 5,
    borderRadius: 100,
    // borderWidth: 2,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Login;
