import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Route,Stock} from './src/navigation/route';

class App extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <NavigationContainer>
        <Stock />
        </NavigationContainer>
      </View>
    );
  }
}

export default App;
